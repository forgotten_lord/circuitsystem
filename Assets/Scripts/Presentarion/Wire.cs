﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Css.Presentarion
{
    [RequireComponent(typeof(MeshRenderer))]
    public class Wire : MonoBehaviour, ICircuit
    {
        [SerializeField]
        public Unit begin;
        [SerializeField]
        public Unit end;
        [SerializeField]
        bool isRevert = false;
        [SerializeField]
        [Range(-0.1f, 0.1f)]
        private float speed = 0;
        [SerializeField]
        [Range(-5f, 5f)]
        public float Voltage = 0;
        MeshFilter MF;
        MeshRenderer MR;
        static float width = 0.1f;
        private Material material;
        public ICircuit circuit;
        public static float layer = 0;
        float length = 0;
        /// <summary>
        /// Коэффициент преобразования координат
        /// </summary>
        public static float ParseFactor = 16;
        public void Init(params object[] parameters)
        {
            Unit unit0 = (Unit)parameters[0];
            Unit unit1 = (Unit)parameters[1];
            begin = unit0; unit0.wires.Add(this);
            end = unit1; unit1.wires.Add(this);
            Repaint();
        }
        static int ParseIntFactor(string param)
        {
            return (int)(((float)int.Parse(param, System.Globalization.NumberStyles.Integer)) / ParseFactor);
        }
        public void StartDraw(Vector3 position)
        {
            Unit unit0 = Instantiate(Resources.Load("Prefabs/Circuits/Unit") as GameObject,
                new Vector3(position.x, position.y, Unit.layer),
                Quaternion.identity,
                CircuitSystem.current.transform).AddComponent<Unit>();
            this.begin = unit0;
            unit0.wires.Add(this);
        }
        public void EndDraw(Vector3 position)
        {
            Unit unit1 = Instantiate(Resources.Load("Prefabs/Circuits/Unit") as GameObject,
                new Vector3(position.x, position.y, Unit.layer),
                Quaternion.identity,
                 CircuitSystem.current.transform).AddComponent<Unit>();
            Wire wire = Instantiate(Resources.Load("Prefabs/Circuits/Wire") as GameObject,
                new Vector3(position.x, position.y, Wire.layer),
                Quaternion.identity,
                 CircuitSystem.current.transform).AddComponent<Wire>();
            wire.end = unit1;unit1.wires.Add(wire);
        }
        public static void Load(string[] parameters)
        {
            Unit unit0 = Instantiate(Resources.Load("Prefabs/Circuits/Unit") as GameObject,
                new Vector3(ParseIntFactor(parameters[1]), ParseIntFactor(parameters[2]), Unit.layer),
                Quaternion.identity,
                CircuitSystem.current.transform).AddComponent<Unit>();
            Unit unit1 = Instantiate(Resources.Load("Prefabs/Circuits/Unit") as GameObject,
                new Vector3(ParseIntFactor(parameters[3]), ParseIntFactor(parameters[4]), Unit.layer),
                Quaternion.identity,
                 CircuitSystem.current.transform).AddComponent<Unit>();
            Wire wire = Instantiate(Resources.Load("Prefabs/Circuits/Wire") as GameObject,
                new Vector3(ParseIntFactor(parameters[1]), ParseIntFactor(parameters[2]), Wire.layer),
                Quaternion.identity,
                 CircuitSystem.current.transform).AddComponent<Wire>();
            wire.begin = unit0; wire.end = unit1;
            unit0.wires.Add(wire); unit1.wires.Add(wire);
        }
        public class ICircuitElement
        {
            public float Voltage = 1f;
            public bool direction = false;
            public float speed = 0.01f;
        }
        private void Start()
        {
            MeshRenderer MR = GetComponent<MeshRenderer>();
            MR.material = new Material(Shader.Find("Unlit/WiresPainter"));
            MR.material.SetTexture("_MainTex", Resources.Load("Textures/Wire3") as Texture);
            material = MR.material;
            name = GetType().Name + CircuitSystem.current.Wires.Count;
            CircuitSystem.current.Wires.Add(this);
            Repaint();
        }
        private void OnDestroy()
        {
            begin.wires.Remove(this);
            end.wires.Remove(this);
            CircuitSystem.current.Wires.Remove(this);
            Destroy(gameObject);
        }
        public void Repaint(float length)
        {
            //Destroy(gameObject.GetComponent<BoxCollider2D>());
            List<Vector3> vertices = new List<Vector3>();
            List<Vector2> uv = new List<Vector2>();
            List<int> triangles = new List<int>();
            float stepsCount = Mathf.FloorToInt(length);
            ///рисуем целую часть
            for (int n = 0; n < stepsCount; n++)
            {
                vertices.AddRange(new Vector3[]
                {
                    new Vector3(n,   width, 5),
                    new Vector3(n+1, width, 5),
                    new Vector3(n+1, -width, 5),
                    new Vector3(n,   -width, 5),
                });
                int m = n * 4;
                uv.AddRange(new Vector2[]
                {
                    new Vector2(0, 0.25f), new Vector2(0, 0.75f), new Vector2(1, 0.75f), new Vector2(1, 0.25f),
                });
                triangles.AddRange(new int[] { m + 0, m + 1, m + 2, m + 0, m + 2, m + 3});
            }
            int count = vertices.Count;
            ///дорисовываем дробную часть
            vertices.AddRange(new Vector3[]
               {
                new Vector3(stepsCount,   width, 5),
                new Vector3(length, width, 5),
                new Vector3(length, -width, 5),
                new Vector3(stepsCount,   -width, 5),
               });
            float result = Mathf.Abs(length - stepsCount);
            uv.AddRange(new Vector2[]
            {
                    new Vector2(0, 0.25f * result), new Vector2(0, 0.75f * result), new Vector2(1, 0.75f * result), new Vector2(1, 0.25f * result),
            });
            triangles.AddRange(new int[] { count + 0, count + 1, count + 2, count + 0, count + 2, count + 3 });
            
           /* Debug.Log("vertices.Count        " + vertices.Count);
            Debug.Log("uv.Count        " + uv.Count);
            Debug.Log("triangles.Count        " + triangles.Count);*/
            MF = gameObject.GetComponent<MeshFilter>();
            if (MF == null)
                MF = gameObject.AddComponent<MeshFilter>();
            MF.mesh = new Mesh()
            {
                vertices = vertices.ToArray(),
                uv = uv.ToArray(),
                triangles = triangles.ToArray(),
            };
            //gameObject.AddComponent<BoxCollider2D>().isTrigger = true;
        }
        public void Drag(Vector3 newPosition)
        {
            begin.transform.position += newPosition;
            end.transform.position += newPosition;
            Repaint();
            //circuit.Drag(newPosition);
        }
        public void Dragged()
        {
            Repaint();
            if (circuit != null)
            {
                circuit.Dragged();
            }
        }
        public void EndDrag()
        {
            Repaint();
            if (circuit != null)
            {
                circuit.Dragged();
            }
        }
        public void Revert()
        {
            Unit u = begin;
            begin = end;
            end = u;
            Drag(Vector3.zero);
        }
        public void Connect(BasicCircuit circuit)
        {
            circuit.Drag(Vector3.zero);
        }
        public void Repaint()
        {
            if (begin != null && end != null)
            {
                Vector3 beginVect = new Vector3(begin.transform.position.x, begin.transform.position.y, Wire.layer);
                Vector3 endVect = new Vector3(end.transform.position.x, end.transform.position.y, Wire.layer);
                //направляем провод в конечную точку
                Vector3 difference = endVect - beginVect;
                transform.eulerAngles = (difference.y > 0)
                    ? new Vector3(0, 0, Vector3.Angle(difference, Vector3.right))
                    : -new Vector3(0, 0, Vector3.Angle(difference, Vector3.right));
                //протягиваем провод в конечную точку
                //transform.localScale = new Vector3(length, CircuitSystem.current.menusScaleDelta, CircuitSystem.current.menusScaleDelta);
                //ставим провод в начальную точку
                transform.position = new Vector3(beginVect.x, beginVect.y, Wire.layer);
                /*Vector3 anti = new Vector3(CircuitSystem.current.transform.localScale.x + 1,
                                           CircuitSystem.current.transform.localScale.y + 1,
                                           CircuitSystem.current.transform.localScale.z + 1);*/
                Repaint((beginVect - endVect).magnitude / CircuitSystem.current.transform.localScale.x);
            }
        }
        public void Iterate()
        {
            Vector2 offset = material.GetTextureOffset("_MainTex");
            if (offset.y > .15f)
            {
                offset = new Vector2(0, -speed);
            }

            if (offset.y < -.15f)
            {
                offset = new Vector2(0, speed);
            }

            offset += new Vector2(0, speed);
            material.SetFloat("_Voltage", Voltage);
            material.SetTextureOffset("_MainTex", offset);
        }
        void Update()
        {
            if (isRevert)
            {
                Revert();
            }
            isRevert = false;
            Iterate();
        }
    }
}