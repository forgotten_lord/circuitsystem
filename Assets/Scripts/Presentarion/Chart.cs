﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using SharpCircuit;
using SharpCircuit.Elements;
using SharpCircuit.Elements.Voltages;
using UnityEngine.EventSystems;
namespace Css.Presentarion
{
    [RequireComponent(typeof(LineRenderer))]
    class Chart : MonoBehaviour
    {
        LineRenderer LR;
        float max = 10f;
        float min = 0f;
        List<Vector3> positions;
        float step = .1f;
        int stepsCountMax = 91;
        float positionX;
        void Start()
        {
            LR = gameObject.GetComponent<LineRenderer>();
            LR.useWorldSpace = false;
            positions = new List<Vector3>();
            positionX = stepsCountMax * step;

            /*var sim = new Circuit();
            var volt0 = sim.Create<VoltageInput>(Voltage.WaveType.DC);
            var ground0 = sim.Create<Ground>();
            var res0 = sim.Create<Resistor>();
            var wire0 = sim.Create<Wire>();

            sim.Connect(volt0.LeadPos, res0.LeadIn);
            sim.Connect(res0.LeadOut, wire0.LeadIn);
            sim.Connect(wire0.LeadOut, ground0.LeadIn);*/
        }
        private void Update()
        {
            NextStep(Mathf.Cos(Time.timeSinceLevelLoad * 8));
        }
        public void NextStep(float value)
        {
            positions.Add(new Vector3(positionX, value, 0));
            for (int n = 0; n < positions.Count; n++)
            {
                positions[n] = positions[n] - new Vector3(step, 0, 0);
                LR.positionCount = positions.Count-1;
            }
            if (positions.Count > stepsCountMax)
                positions.RemoveAt(0);
            LR.SetPositions(positions.ToArray());
        }
    }
}