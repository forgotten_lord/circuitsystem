﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Css.Presentarion
{
    public interface ICircuit
    {
        /// <summary>
        /// Перетаскивание элемента
        /// </summary>
        /// <param name="newPosition">разница между старым и новым положением</param>
        void Drag(Vector3 newPosition);
        void EndDrag();
        void Dragged();
        //string Save();
        /// <summary>
        /// Начинаем рисовать объект
        /// </summary>
        void StartDraw(Vector3 position);
        /// <summary>
        /// Заканчиваем рисовать объект
        /// </summary>
        void EndDraw(Vector3 position);
        void Init(params object[] parameters);
    }
}
