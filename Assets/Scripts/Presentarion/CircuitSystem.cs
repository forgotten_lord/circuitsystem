﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Css.TextFileParser;
namespace Css.Presentarion
{
    public class CircuitSystem : MonoBehaviour
    {
        public static CircuitSystem current;
        [SerializeField]
        public List<BasicCircuit> Circuits = new List<BasicCircuit>();
        [SerializeField]
        public List<Wire> Wires = new List<Wire>();
        [SerializeField]
        public List<Unit> Units = new List<Unit>();
        public float scaleDelta = 1;
        public float menusScaleDelta = 1;
        float scalingMin = 0.5f;
        float scalingMax = 4f;
        [SerializeField]
        bool isStressTest = false;
        void Awake()
        {
            current = this;
            scaleDelta = transform.localScale.x;
        }
        void Load()
        {
            CircuitSystem.current.Clear();
        }
        void Save()
        {

        }
        /// <summary>
        /// Альтернатива Update
        /// </summary>
        void UpdateWires()
        {
            for (int n = 0; n < Wires.Count; n++)
            {
                Wires[n].Iterate();
            }
        }
        //Stress test
        int count = 0;
        int max = 0;
        void Update()
        {
            UpdateWires();
            if (isStressTest)
                if (count < max)
                {
                    for (int n = 0; n < 20; n++)
                        CreateCapacitor(new Vector3(n * 4, count, 0));
                    count++;
                }
                else isStressTest = false;
        }
        public void Clear()
        {
            for(int c=0;c<Circuits.Count;c++)
                Destroy(Circuits[c]);
            Circuits.Clear();
            for (int w = 0; w < Wires.Count; w++)
                Destroy(Wires[w]);
            Wires.Clear();
            for (int u = 0; u < Units.Count; u++)
                Destroy(Units[u]);
            Units.Clear();
        }
        public void RunTest()
        {
            max += 20;
            isStressTest = true;
        }
        public Wire CreateWire(Vector3 position)
        {
            Unit unit0 = Instantiate(Resources.Load("Prefabs/Circuits/Unit") as GameObject,
                position - new Vector3(-2, 0, Unit.layer),
                Quaternion.identity,
                transform).AddComponent<Unit>();
            Unit unit1 = Instantiate(Resources.Load("Prefabs/Circuits/Unit") as GameObject,
                position - new Vector3(2, 0, Unit.layer),
                Quaternion.identity,
                transform).AddComponent<Unit>();
            Wire wire = Instantiate(Resources.Load("Prefabs/Circuits/Wire") as GameObject,
                position - new Vector3(-2, 0, Wire.layer),
                Quaternion.identity,
                transform).AddComponent<Wire>();
            wire.begin = unit0; wire.end = unit1;
            unit0.wires.Add(wire); unit1.wires.Add(wire);
            return wire;
        }
        public void CreateCapacitor(Vector3 position)
        {
            Capacitor capacitor = Instantiate(Resources.Load("Prefabs/Circuits/Capacitor") as GameObject,
                position - new Vector3(0, 0, BasicCircuit.layer),
                Quaternion.identity,
                transform).AddComponent<Capacitor>();
            capacitor.Restore(CreateWire(position));
        }
        public void ScalingElements(float scale)
        {
            //масштабируем саму схему
            scaleDelta = Mathf.Clamp(transform.localScale.x+scale * 0.1f, 0.5f, 4f);
            menusScaleDelta = Mathf.Clamp(1/scaleDelta, scalingMin, scalingMax);
            transform.localScale = new Vector3(scaleDelta, scaleDelta, 1);
            foreach (Unit unit in Units)
            {
                unit.transform.localScale = new Vector3(menusScaleDelta, menusScaleDelta, 1);
            }
            foreach (Wire wire in Wires)
            {
                wire.transform.localScale = new Vector3(wire.transform.localScale.x, menusScaleDelta, 1);
            }
            foreach (BasicCircuit circuit in Circuits)
            {
                circuit.transform.localScale = new Vector3(menusScaleDelta, menusScaleDelta, 1);
            }
        }
    }
}