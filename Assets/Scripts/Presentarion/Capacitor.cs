﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Css.Circuits;
namespace Css.Presentarion
{
    public class Capacitor : BasicCircuit, ICircuit
    {
        [SerializeField]
        public Wire wire;
        //BoxCollider2D boxCollider;
        public Capacitor(params object[] parameters)
        {
        }
        void Start()
        {
            //boxCollider = gameObject.AddComponent<BoxCollider2D>();
            name = GetType().Name + CircuitSystem.current.Circuits.Count;
            CircuitSystem.current.Circuits.Add(this);
            Drag(Vector3.zero);
        }
        private void OnDestroy()
        {
            CircuitSystem.current.Circuits.Remove(this);
            wire.circuit = null;
            Destroy(gameObject);
        }
        public void Restore(Wire _wire)
        {
            wire = _wire;
            wire.circuit = this;
            Drag(Vector3.zero);
        }
        public void Load(string data)
        {

        }
        public void Dragged()
        {
            if (wire == null || wire.transform.localScale.x == 0)
            {
                Destroy(this);
            }
            else
            {
                Vector3 beginPos = wire.begin.transform.localPosition;
                Vector3 endPos = wire.end.transform.localPosition;
                Vector3 center = (beginPos + endPos) / 2;
                Vector3 difference = endPos - beginPos;
                transform.rotation = wire.transform.rotation;
                transform.localPosition = new Vector3(center.x, center.y, layer);
            }
        }
        public void EndDrag()
        {
        }
        public void StartDraw(Vector3 position)
        {
        }
        public void EndDraw(Vector3 position)
        {
        }
    }
}