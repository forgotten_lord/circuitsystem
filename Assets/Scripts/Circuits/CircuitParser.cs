﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Css.Presentarion;
using System.Reflection;
namespace Css.Circuits
{
    public class CircuitParser : MonoBehaviour
    {
        static Dictionary<string, Type> Elements = new Dictionary<string, Type>();
        /// <summary>
        /// Получаем классы в пространстве Css.Presentarion предназначенные для маппинга
        /// </summary>
        private void Start()
        {
            Type[] typelist = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.Namespace == "Css.Presentarion").ToArray();
            foreach (Type type in typelist)
            {
                foreach (MemberInfo method in type.GetMembers())
                {
                    if (method.Name == "DumpType" && method.MemberType == MemberTypes.Method)
                    {
                        Elements.Add(((MethodInfo)method).Invoke(this, null).ToString(), type);
                        break;
                    }
                }
            }
            foreach (var element in Elements)
            {
                string str = "methods:";
                foreach (var method in element.Value.GetMethods())
                {
                    str += "         "+method.Name;
                }
                Debug.Log(element.Key + "     " + element.Value + str);
            }
        }
        /*public static void Parse(string parameters)
        {
            string[] splitedParams = parameters.Split(' ');

            if (Elements.ContainsKey(splitedParams[0]))
            {
                print("parsing:...  " + parameters);        
                MethodInfo method = (Elements[splitedParams[0]]).GetMethod("Load");
                object classInstance = Activator.CreateInstance(null, Elements[splitedParams[0]]);
                method.Invoke(classInstance, splitedParams);
            }
            else
            {
                print("unknown type:...  " + parameters);
            }
        }*/
        public static ICircuit Parse(string parameters)
        {
            string[] splitedParams = parameters.Split(' ');
            string str = null;
            for (int n = 0; n < splitedParams.Length; n++)
                str += splitedParams[n] + "     ";
            switch (splitedParams[0])
            {
                case "$": break;
                case "w":
                case "W": Wire.Load(splitedParams); break;
                case "r": break;
                case "c": break;
                case "209": break;
                case "l": break;
                case "s": break;
                case "S": break;
                case "174": break;
                case "169": break;
                case "171": break;
                case "178": break;
                case "m": break;
                case "187": break;
                case "g": break;
                case "v": break;
                case "R": break;
                case "170": break;
                case "172": break;
                case "A": break;
                case "200": break;
                case "201": break;
                case "i": break;
                case "n": break;
                case "O": break;
                case "181": break;
                case "x": break;
                case "p": break;
                case "216": break;
                case "207": break;
                case "368": break;
                case "370": break;
                case "210": break;
                case "211": break;
                case "d": break;
                case "z": break;
                case "162": break;
                case "t": break;
                case "f": break;
                case "j": break;
                case "177": break;
                case "400": break;
                case "175": break;
                case "173": break;
                case "a": break;
                case "159": break;
                case "160": break;
                case "180": break;
                case "182": break;
                case "183": break;
                case "179": break;
                case "401": break;
                case "402": break;
                case "212": break;
                case "213": break;
                case "214": break;
                case "215": break;
                case "155": break;
                case "156": break;
                case "193": break;
                case "157": break;
                case "197": break;
                case "184": break;
                case "186": break;
                case "164": break;
                case "163": break;
                case "168": break;
                case "188": break;
                case "196": break;
                case "158": break;
                case "195": break;
                case "!": break;
                case "165": break;
                case "161": break;
                case "166": break;
                case "167": break;
                case "194": break;
                default: return null;
            }
            return null;
        }
    }
}
