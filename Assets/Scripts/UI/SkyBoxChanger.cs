﻿using UnityEngine;
namespace Css.UI
{
    public class SkyBoxChanger : MonoBehaviour
    {
        void Start()
        {
            RenderSettings.skybox = Resources.Load("Materials/SkyBox") as Material;
        }
    }
}