﻿using Css.Circuits;
using Css.Presentarion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;
namespace Css.TextFileParser
{
    public class FileParser
    {
        public static void OpenFile(string filePath)
        {
            using (StreamReader reader = new StreamReader(File.OpenRead(filePath)))
            {
                while (!reader.EndOfStream)
                {
                    string parameters = reader.ReadLine();
                    if (parameters != null)
                    {
                        CircuitSystem.current.Clear();
                        CircuitParser.Parse(parameters);
                    }
                }
            }
        }
    }
}
