﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Css.Presentarion;
using UnityEngine;
namespace Css.Presentarion
{
    public class Unit : MonoBehaviour, ICircuit
    {
        private BoxCollider2D boxCollider;
        public List<Wire> wires = new List<Wire>();
        public List<BasicCircuit> circuits = new List<BasicCircuit>();
        public static float layer = -10;
        private Material material;
        public void Init(params object[] parameters)
        {
            Drag((Vector3)parameters[0]);
            EndDrag();
        }
        public virtual void Start()
        {
            boxCollider = gameObject.AddComponent<BoxCollider2D>();
            MeshRenderer MR = gameObject.AddComponent<MeshRenderer>();
            boxCollider.offset = new Vector2();
            boxCollider.size = new Vector2(2f, 2f);
            transform.localPosition = Vector3Int.RoundToInt(transform.localPosition);
            name = GetType().Name + CircuitSystem.current.Units.Count;
            CircuitSystem.current.Units.Add(this);
            MR.material = new Material(Shader.Find("Unlit/WiresPainter"));
            MR.material.SetTexture("_MainTex", Resources.Load("Textures/Wire3") as Texture);
            material = MR.material;
            name = GetType().Name + CircuitSystem.current.Wires.Count;
            Repaint();
            EndDrag();
        }
        private void OnDestroy()
        {
            wires.Clear();
            CircuitSystem.current.Units.Remove(this);
            Destroy(gameObject);
        }
        public void Drag(Vector3 newPosition)
        {
            transform.position += newPosition;
            float newX = transform.position.x;
            float newY = transform.position.y;
            transform.position = new Vector3(newX, newY, Unit.layer);
            foreach (Wire wire in wires)
            {
                wire.Dragged();
            }
        }
        public virtual void Dragged()
        {
        }
        public virtual void EndDrag()
        {
            transform.localPosition = Vector3Int.RoundToInt(new Vector3(transform.localPosition.x, transform.localPosition.y, 0));
            KillNeighboors();
            foreach (Wire wire in wires)
            {
                wire.EndDrag();
            }
        }
        public void Connect(Wire w, bool toBegin)
        {
            if (toBegin)
            {
                w.begin = this;
            }
            else
            {
                w.end = this;
            }

            wires.Add(w);
        }
        /// <summary>
        /// Удаляем парные точки
        /// </summary>
        public void KillNeighboors()
        {
            List<Unit> Neighboors = CircuitSystem.current.Units.Where(u =>
            (u.transform.position.x == transform.position.x) &&
            (u.transform.position.y == transform.position.y)).ToList();
            Neighboors.Remove(this);
            foreach (Unit u in Neighboors)
            {
                foreach (Wire w in u.wires)
                {
                    Connect(w, w.begin == u);
                }
                Destroy(u);
            }
        }
        public void Repaint()
        {
            //Destroy(gameObject.GetComponent<BoxCollider2D>());
            List<Vector3> vertices = new List<Vector3>() {
                                new Vector3(0f, 0.2f, -0.8f),
                                new Vector3(0.141421f, 0.141421f, -0.8f),
                                new Vector3(-0.2f, 0f, -0.8f),
                                new Vector3(-0.141421f, 0.141421f, -0.8f),
                                new Vector3(0.2f, -1E-06f, -0.8f),
                                new Vector3(-0.141421f, -0.141422f, -0.8f),
                                new Vector3(0.141421f, -0.141422f, -0.8f),
                                new Vector3(0f, -0.200001f, -0.8f),};
            List<Vector2> uv = new List<Vector2>() {
                                new Vector2(0.692109f,0.769238f),
                                new Vector2(0.688868f,0.769238f),
                                new Vector2(0.6944f,0.789011f),
                                new Vector2(0.6944f,0.777428f),
                                new Vector2(0.686576f,0.777428f),
                                new Vector2(0.692109f,0.797202f),
                                new Vector2(0.686576f,0.789011f),
                                new Vector2(0.688868f,0.797202f),};
            List<int> triangles = new List<int>() {0,1,2,0,2,3,4,2,1,4,5,2,6,5,4,6,7,5};

            MeshFilter MF = gameObject.GetComponent<MeshFilter>();
            if (MF == null)
                MF = gameObject.AddComponent<MeshFilter>();
            MF.mesh = new Mesh()
            {
                vertices = vertices.ToArray(),
                uv = uv.ToArray(),
                triangles = triangles.ToArray(),
            };
            //gameObject.AddComponent<BoxCollider2D>().isTrigger = true;
        }
        public void StartDraw(Vector3 position)
        {
        }
        public void EndDraw(Vector3 position)
        {
        }
    }
}