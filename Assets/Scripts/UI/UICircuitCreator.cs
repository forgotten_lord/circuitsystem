﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Css.TextFileParser;
using Css.Presentarion;
namespace Css.UI
{
    class UICircuitCreator : MonoBehaviour
    {
        void Start()
        {
            //for (int n = 0; n < transform.childCount; n++)
            transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(CreateWire);
            transform.GetChild(1).gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(CreateCapacitor);
            transform.GetChild(2).gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(RunTest);/*
            transform.GetChild(1).gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(CreateCapacitor);
            transform.GetChild(2).gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(CreateWire);
            transform.GetChild(3).gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(CreateWire);
            transform.GetChild(4).gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(CreateWire);
            transform.GetChild(5).gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(CreateWire);*/
        }
        void CreateWire()
        {
            CircuitSystem.current.CreateWire(new Vector3(0, 0, 0));
        }
        void CreateCapacitor()
        {
            CircuitSystem.current.CreateCapacitor(new Vector3(0, 0, 0));
        }
        void RunTest()
        {
            CircuitSystem.current.RunTest();
        }
    }
}