﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Css.Presentarion;
using UnityEngine;
using UnityEngine.EventSystems;
namespace Css.UI
{
    [RequireComponent(typeof(UnityEngine.UI.Text))]
    public class PopUpMenuOption : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public Action action;
        [SerializeField]
        PopUpSubMenu SubMenu;
        static PopUpSubMenu lastOpenedSubMenu;
        public static Color backGroundColor = new Color(.2f, .2f, .2f, .2f);
        public static Color notSelestedColor = new Color(.5f, .5f, .5f,1);
        public static Color SelestedColor = Color.white;
        UnityEngine.UI.Text textUI;
        void Start()
        {
            textUI = GetComponent<UnityEngine.UI.Text>();
            textUI.color = notSelestedColor;
        }
        public void OnPointerClick(PointerEventData eventData)
        {
            action += PresentationCircuitSpawner.Call;
            action();
        }
        public void OnPointerEnter(PointerEventData eventData)
        {
            textUI.color = SelestedColor;
            ShowNextSubMenu(SubMenu);
        }
        public void OnPointerExit(PointerEventData eventData)
        {
            textUI.color = notSelestedColor;
        }
        void ShowNextSubMenu(PopUpSubMenu submenu)
        {
            if (submenu != null)
            {
                if (lastOpenedSubMenu != null)
                {
                    lastOpenedSubMenu.gameObject.SetActive(false);
                }
                Vector3 screenPos = Camera.main.ScreenToViewportPoint(transform.position + new Vector3(((RectTransform)transform).rect.width / 2, 0, 0));
                SubMenu.transform.position = screenPos;
                submenu.gameObject.SetActive(true);
                lastOpenedSubMenu = submenu;
            }
        }
    }
}