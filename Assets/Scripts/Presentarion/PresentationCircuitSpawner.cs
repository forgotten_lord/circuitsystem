﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
namespace Css.Presentarion
{
    class PresentationCircuitSpawner
    {
        /*/// <summary>
        /// Универсальный расстановщик компонентов (это не конструктор!)
        /// </summary>
        /// <param name="monoBehaviour">тип компонента</param>
        /// <param name="parameters">параметры инициализации</param>
        /// <returns></returns>
        public static Component Spawn(MonoBehaviour monoBehaviour, params object[] parameters)
        {
            GameObject GO = new GameObject();
            GO.transform.parent = CircuitSystem.current.transform;
            Type scriptType = monoBehaviour.GetType();
            Component component = GO.GetComponent(scriptType);
            if (component == null)
                component = GO.AddComponent(scriptType);
            component.SendMessage("Init", parameters);
            return component;
        }*/
        /// <summary>
        /// Универсальный расстановщик компонентов (это не конструктор!)
        /// </summary>
        /// <param name="monoBehaviour">тип компонента</param>
        /// <param name="parameters">параметры инициализации</param>
        /// <returns></returns>
        public static Component Spawn(MonoBehaviour monoBehaviour, params object[] parameters)
        {
            GameObject GO = new GameObject();
            GO.transform.parent = CircuitSystem.current.transform;
            Type scriptType = monoBehaviour.GetType();
            Component component = GO.GetComponent(scriptType);
            if (component == null)
                component = GO.AddComponent(scriptType);
            component.SendMessage("Init", parameters);
            return component;
        }
        public static void Call()
        {
            Spawn(new Wire(), new object[] { Spawn(new Unit(), new object[] { new Vector3() }),
                                             Spawn(new Unit(), new object[] { new Vector3(5,5,0) })});
            var asrgd = Spawn(new Capacitor(), new object[] { Spawn(new Wire(), new object[] {
                                             Spawn(new Unit(), new object[] { new Vector3() }),
                                             Spawn(new Unit(), new object[] { new Vector3(5,5,0) })}) });
            Debug.Log(asrgd.name);
        }
    }
}