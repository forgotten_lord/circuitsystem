﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Css.Presentarion;
using Css.UI;
using UnityEngine;
using UnityEngine.EventSystems;
namespace Css.UI
{
    [RequireComponent(typeof(UnityEngine.UI.Image))]
    public class PopUpSubMenu : MonoBehaviour
    {
        PopUpSubMenu SubMenu;
        static PopUpSubMenu lastOpenedSubMenu;
        UnityEngine.UI.Image imageUI;
        void Start()
        {
            imageUI = GetComponent<UnityEngine.UI.Image>();
            PopUpMenu.subMenus.Add(this);
        }
    }
}