﻿Shader "Unlit/WiresPainter"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
        //[Toggle(IsSelected)]
		_Voltage("Voltage", Float) = 0
		Speed ("IsSelected", Float) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

            #pragma shader_feature IsSelected
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float IsSelected;
			float _Voltage;
			float time;
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				/*time = max(abs(_SinTime.x-.5) , abs(_SinTime.x));
				o.uv = (TRANSFORM_TEX(v.uv, _MainTex).y + time);*/
				o.uv = TRANSFORM_TEX(v.uv, _MainTex)
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			float rand(float myVector)  {
				return frac(sin( dot(myVector ,float3(12.9898,78.233,45.5432) )) * 43758.5453);
			}
			fixed4 frag (v2f i) : SV_Target
			{
				float4 col = tex2D(_MainTex, i.uv);
				if (col.a < .1) discard;
				if (col.r < .1)
				{
					if (_Voltage > 1)
					col = float4(.0,_Voltage,.0,1);
					else
					if (_Voltage < -1)
					col = float4(-_Voltage,.0,.0,1);
					else
					{
					col = float4(.5,.5,.5,1);
					}
				}
				else
				{
					col = float4(1,1,0,1);
				}
				//col.b = min(col.r,col.g);
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
