﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Css.TextFileParser;
namespace Css.UI
{
    class OpenDialog : MonoBehaviour
    {
        [SerializeField]
        UnityEngine.UI.Button openFileButton;
        [SerializeField]
        UnityEngine.UI.InputField FilePathInputField;
        void Start()
        {
            openFileButton.onClick.AddListener(OpenFileButtonPressed);
        }
        void OpenFileButtonPressed()
        {
            if (File.Exists(FilePathInputField.text))
                FileParser.OpenFile(FilePathInputField.text);
        }
    }
}
