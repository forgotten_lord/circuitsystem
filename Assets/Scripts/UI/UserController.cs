﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Css.Presentarion;
using Css.UI;
using UnityEngine;
namespace Css.UI
{
    public class UserController : MonoBehaviour
    {
        private new Camera camera;
        private Vector3 mousePos;
        private ICircuit capturedObject = null;
        private ICircuit createdCircuit = null;
        void Start()
        {
            camera = GetComponent<Camera>();
        }
        void Update()
        {
            float MouseScrollWheel = Input.GetAxis("Mouse ScrollWheel");
            if (MouseScrollWheel != 0)
            {
                CircuitSystem.current.ScalingElements(MouseScrollWheel);
                //camera.orthographicSize += Input.GetAxis("Mouse ScrollWheel");
            }

            Vector3 mouseWPos = camera.ScreenToWorldPoint(Input.mousePosition);
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
                if (hit.transform == null)
                {
                    mousePos = mouseWPos;
                    capturedObject = null;
                }
                else
                {
                    ICircuit unit = hit.transform.gameObject.GetComponent<Unit>();
                    if (unit != null && unit is ICircuit)
                    {
                        capturedObject = unit;
                        mousePos = mouseWPos;
                    }
                }
            }

            if (Input.GetMouseButton(0))
            {
                if (capturedObject == null)
                {
                    Vector3 result = mousePos - mouseWPos;
                    result = new Vector3(result.x, result.y, 0);
                    CircuitSystem.current.transform.position -= result;
                    mousePos = mouseWPos;
                }
                else
                {
                    //Debug.Log("camera.transform.position"+ camera.transform.position + "        mousePos" + mousePos + "        mouseWPos" + mouseWPos);
                    capturedObject.Drag(mouseWPos- mousePos);
                    /*if (capturedObject is Unit)
                    {
                        Debug.Log("isUnit!!!");
                    }
                    if (capturedObject is Wire)
                    {
                        Debug.Log("isWire!!!");
                    }
                    if (capturedObject is Circuit)
                    {
                        Debug.Log("isCircuit!!!");
                    }*/
                    mousePos = mouseWPos;
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (capturedObject != null)
                {
                    //capturedObject.transform.position = Vector3Int.RoundToInt(-(mousePos - Input.mousePosition) * .01f);
                    capturedObject.EndDrag();
                    capturedObject = null;
                    mousePos = Vector3.zero;
                    mouseWPos = Vector3.zero;
                }
            }

            if (Input.GetMouseButtonDown(1))
            {

            }
            if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                DialogsController.current.dialogNum = -1;
            }

            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                DialogsController.current.dialogNum = -1;
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                DialogsController.current.dialogNum = 0;
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                DialogsController.current.dialogNum = 1;
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                DialogsController.current.dialogNum = 1;
            }
        }
    }
}
