﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Css.TextFileParser;
namespace Css.UI
{
    class DialogsController : MonoBehaviour
    {
        [SerializeField]
        List<GameObject> dialogs;
        public static DialogsController current;
        [Range(-1, 2)]
        [SerializeField]
        public int dialogNum = -1;
        void Start()
        {
            current = this;
            /* dialogs =new List<GameObject>();
            for (int n=0; n<transform.childCount;n++)
            {
                dialogs.Add(transform.GetChild(n).gameObject);
            }*/
            ShowDialogByNum(0);
        }
        void ShowDialogByNum(int DialogNum)
        {
            for (int num=0; num<dialogs.Count;num++)
            {
                dialogs[num].SetActive(DialogNum == num);
            }
        }
        void Update()
        {
            ShowDialogByNum(dialogNum);
        }
    }
}