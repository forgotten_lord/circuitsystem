﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Presentarion
{
    [Flags]
    public enum CircuitStates
    {
        /// <summary>
        /// Элемент под напряжением
        /// </summary>
        isAlive,
        /// <summary>
        /// Элемент выбран
        /// </summary>
        isSelected
    }
}
