﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Css.Circuits;
using UnityEngine.EventSystems;
namespace Css.Presentarion
{
    [RequireComponent(typeof (UnityEngine.UI.Text))]
    public class VoltageSource : Unit, ICircuit, IPointerEnterHandler, IPointerDownHandler
    {
        public static float layer = -20;
        //BoxCollider2D boxCollider;
        UnityEngine.UI.Text source;
        Wire wire;
        public void Start()
        {
            source = gameObject.GetComponent<UnityEngine.UI.Text>();
            wire.begin = this;
        }
        public void Drag(Vector3 newPosition)
        {
            transform.position += newPosition;
            float newX = transform.position.x;
            float newY = transform.position.y;
            transform.position = new Vector3(newX, newY, Unit.layer);
            foreach (Wire wire in wires)
            {
                wire.Dragged();
            }
        }
        public void Dragged()
        {
        }
        public override void EndDrag()
        {
            transform.localPosition = Vector3Int.RoundToInt(new Vector3(transform.localPosition.x, transform.localPosition.y, 0));
        }
        public void StartDraw(Vector3 position)
        { }
        public void EndDraw(Vector3 position)
        { }
        public void OnPointerClick(PointerEventData eventData)
        {
        }
        public void OnPointerEnter(PointerEventData eventData)
        {
        }
        public void OnPointerDown(PointerEventData eventData)
        {
        }
    }
}