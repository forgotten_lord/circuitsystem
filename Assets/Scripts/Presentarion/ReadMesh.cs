﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
namespace Css.Presentarion
{
    public class ReadMesh : MonoBehaviour
    {
        void Start()
        {
            MeshFilter MF = GetComponent<MeshFilter>();
            Vector3[] vertices = MF.mesh.vertices;
            Vector2[] uv = MF.mesh.uv;
            int[] triangles = MF.mesh.triangles;
            using (StreamWriter sw = new StreamWriter(@"E:\Mesh.txt"))
            {
                StringBuilder outputStr = new StringBuilder("vertices: ");
                for (int n = 0; n < vertices.Length; n++)
                    sw.WriteLine("new Vector3(" + vertices[n].x + "f, " + vertices[n].y + "f, " + vertices[n].z + "f),");
                sw.WriteLine();
                for (int n = 0; n < uv.Length; n++)
                    sw.WriteLine("new Vector2("+uv[n].x + "f," + uv[n].y + "f), ");

                for (int n = 0, line = 0; n < triangles.Length; n++, line++)
                {
                    if (line == 16) sw.WriteLine();
                    sw.Write(triangles[n] + ",");
                }
            }

        }
    }
}
